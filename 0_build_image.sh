
function build_image() {
    echo "Building image..."

    docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

    # if [ "$site" = "test" ]; then
    #     echo "Building test image..."
    # else
    #     echo "Building staging image..."
    # fi

    if [ -n "$CI_COMMIT_TAG" ]; then
        echo "Tag exists, building and pushing tagged image..."
        docker build -t $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG .
        docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
    else
        echo "No tag found, building and pushing commit sha image..."
        docker build -t $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA .
        docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    fi

    echo "Done"
}

echo "Get opts ..."

while [[ "$#" -gt 0 ]]; do
    case $1 in
        --site) site="$2"; shift ;;
        --deploy) deploy="$2"; shift ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done

echo "Site is set to: $site"
echo "Deploy is set to: $deploy"

build_image
